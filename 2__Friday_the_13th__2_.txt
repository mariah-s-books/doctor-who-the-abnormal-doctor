Nick had went out to find a wheelchair for The Doctor while she sat on the couch. She hadn't saw herself yet and that kinda made her sad. Usually she was fighting an alien by now. Nick opened the door and forced a wheelchair up the stairs. "I finally found one. Surprisingly, they were hard to find," he said, rolling it to her.

"What do you mean they were hard to find?" She asked.

"Like, they were all gone," he replied.

"Oh no. I remember this. What is the date?" She questioned.

"Friday, October 13th,"

"Year?"

"2023,"

"Oh yes. Put me in the wheelchair now!" The Doctor knew what happened on that day. Nick transferred her to the wheelchair. "Where's your bathroom?"

"Down the hall, first door on the right," he replied. She rolled to the bathroom and looked in the mirror. The Doctor had brown hair, blue eyes, and she was wearing a muddy robe.

'A robe, I'm wearing a robe! No, no, no. Not a robe. Wait, why not? Hm, robes are uncool?' She clapped her hands. 'Yes. I need a new wardrobe,' she rolled to the living room.

"So, what's today?" Nick asked.

"The Daleks and the Empty Children team up. You have a car?" The Doctor asked.

"Yes, wh-"

"Take me to the mall,"

***

Nick and The Doctor were in Macy's, in the mall. They were in the women's suits section. "So, what are the Daleks and the Empty Kids or whatever?" Nick asked.

"Empty Children..."

"Whatever," he replied.

"They're aliens," she said. She grabbed a white dress shirt.

"Aliens aren't real," he stated.

"Then what am I?" She undid her robe.

"You're a- wait, what are you doing?" He asked.

"Oi, turn around," She replied. He turned around and The Doctor put the shirt on. She dug through the clothes until she found a brown, buttoned up, vest. 'Perfect!' She thought. She put it on and buttoned it up. 'Now the coat,' She dug through a different rack and picked out a lighter shade of brown coat. She put it on. She rolled to the mirror. 'Yes! I look good,' she ran to the ties and tied one on her neck. She looked at a mirror. 'Nah. A bowtie would be better,' she untied the tie and set it back. She grabbed a red bowtie and tied it. 'This is it. Now the pants,' The Doctor rolled to the pants and threw her robe. She grabbed black dress pants and put them on. 'Oh. I make this look good,' She grabbed her Sonic Screwdriver and put it in the jacket pocket. She rolled over to the shoes and grabbed black dress shoes. She put them on and rolled over to Nick.

"Doctor. You look great," he said.

"Thank you. Now..."

"Are you my mommy?" A kid said.

"Ex-term-in-ate," A Dalek said.

"It's starting, c'mon," The Doctor showed Nick. They rushed back to his house and The Doctor ran to her TARDIS. She opened the door. The TARDIS interior had a deep blue outline between the white round things that were putting off light. It also had wheelchair ramps going up and down the levels. 'It's...beautiful,' she rolled inside, looking at the view.

"It's...bigger on the inside," Nick said. The Doctor turned around.

"No...really? I never known. Thank you, Captain Obvious," she replied.

"Hey!" He said. She laughed and rolled to the TARDIS console. The TARDIS console was white with a deep blue on the bottom half of it. The computer screen on the glass where the beam of light is. (A/N: I want to describe more but I have literally no idea what the parts are). The screen was blinking, 'CHECK THE CONSOLE DRAWER' in blue. The Doctor opened the drawer and there was a needle with a chip on the end.

'Yes. This might sync my voice to the TARDIS and that'll relay my thoughts to speech. Thank you, old girl,' She smiled and injected the chip into her arm with a sight gasp. The computer screen changed to a red, 'UPLOADING DATA. PLEASE WAIT' on it. After a minute, the screen showed a green, 'DATA TRANSFER COMPLETE. YOU CAN NOW SPEAK' text. "I can actually speak now?" she asked.

"Apparently. What is this place?" Nick replied. She turned to face him.

"The TARDIS, Time And Relative Dimensions In Space. It's my time machine," She said.

"No way. That's why you asked for the year," he answered. She chuckled and the phone started ringing. "There's a phone?"

"Of course. This is a police public call box," the phone was on the console and she picked it up.

"Are you my..."

"Who are you? Why are you back?" The Doctor asked.

"Hel-lo, Doc-tor. The end is near," the line went silent and she put the phone back on the console.

***

"Doc-tor," A Dalek said behind The Doctor. The Doctor turned around.

"What are you doing? What is your plan?" She asked.

"Planet ex-term-in-at-ion," it said.

"Yeah. You tried that but failed," She replied.

"Dal-eks will not fail," it answered. A bunch of Empty Children grouped around them.

"Are you my mommy?" They all said. The Doctor pointed her Sonic Screwdriver at the Dalek and turned it on. It blew up. She faced the Empty Children.

"Who is your mommy?" She yelled.

"I want my mommy,"

"I know! Who is your mommy? This would be a lot easier if  you just tell me!"

"Jackie," one said.

"Jackie, who,"

"Tyler," The Doctor ran to a house and knocked on the door. The door opened and an woman opened it.

"I'm The Doctor, hi again. I need you," The Doctor said.

"Mommy?" One of Empty Children said.

Jackie gasped. "Rose?"

"Yes, Mommy," it said.

"Wait? This is 2023. How is Rose a kid?" The Doctor questioned. The Empty Child jumped in Jackie's arms and it's gas mask started fading. Now the child is a normal little girl with blonde hair.

"Thanks, Doctor," Rose said. All the gas masks on the children started fading and The Doctor ran into her TARDIS. She flipped some buttons and switched the demateralize switch on.



